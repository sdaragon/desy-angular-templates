import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'desy-angular-templates';
  footerHtml = 'Creado por <a href="https://sda.aragon.es/" class="c-link c-link--neutral">SDA Servicios Digitales de Aragón</a>';

  public constructor(private titleService: Title, private route: ActivatedRoute) { }

  ngOnInit() {
    this.removeMainSpinner();

    /*
     * Realiza un scroll automáticamente cuando se realiza una navegación dentro de la misma página.
     * Funciona mejor que especificar el anchorScrolling en el routerOptions del AppRoutingModule.
     */
    this.route.fragment.subscribe(fragment => {
      if (fragment) {
        const elem = document.querySelector('#' + fragment);
        if (elem) {
          elem.scrollIntoView({behavior: "smooth"});
        }
      }
    });
  }

  /**
   * Elimina el spinner principal
   */
  private removeMainSpinner(): void {
    const spinner = document.getElementById('initial-spinner');
    if (spinner) {
      const spinnerDiv = spinner.firstElementChild;
      const spinnerParagraph = spinnerDiv?.firstElementChild;
      if (spinnerDiv && spinnerParagraph) {
        spinnerParagraph.textContent = 'El contenido de la pagina se ha cargado';
        spinnerDiv.classList.add('sr-only');
      }

      // Si se desea cambiar el foco, conviene realizarlo con un ligero desfase para dar tiempo al lector de pantalla a
      // leer el cambio de texto
      // setTimeout( () => {
      //   const focustableElements = document.body.querySelectorAll('button, a, input, [tabindex="0"], select, textarea');
      //   focustableElements.item(0).focus();
      // }, 100);

      setTimeout(() => spinner.remove(), 5000);
    }
  }
}
