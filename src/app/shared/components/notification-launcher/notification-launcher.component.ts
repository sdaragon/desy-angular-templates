import { Component, ComponentRef, TemplateRef } from '@angular/core';
import { AlertComponent, AlertOptions, AlertService, NotificationOptions } from 'desy-angular';
import { AlertAreasService } from '../../services/alert-areas.service';

@Component({
  selector: 'app-notification-launcher',
  templateUrl: './notification-launcher.component.html',
  styleUrls: ['./notification-launcher.component.css']
})
export class NotificationLauncherComponent {

  constructor(private alertAreasService: AlertAreasService, private alertService: AlertService) { }

  openHeaderNotification(): void {
    this.openNotification(AlertAreasService.HEADER_NOTIFICATION_AREA_KEY, 'c-notification--success mt-base');
  }

  openFooterNotification(): void {
    this.openNotification(AlertAreasService.FOOTER_NOTIFICATION_AREA_KEY, 'c-notification--success mt-base');
  }

  openInnerContainerNotification(): void {
    this.openNotification(AlertAreasService.INNER_CONTENT_NOTIFICATION_AREA_KEY, 'c-notification--success mb-base');
  }

  openNotification(alertAreaKey: string, notificationClasses: string) {
    const alertArea = this.alertAreasService.alertAreas.get(alertAreaKey);
    if (alertArea) {
      const alertOptions: AlertOptions = {
        id: 'alert',
        place: alertArea,
        role: 'alert',
        ariaLive: 'assertive',
        focusFirst: false
      };

      /*
       * Al ser una notificación estándar, puede especificarse mediante el objeto NotificationOptions, pero el servicio
       * admite contenido más personalizado.
       */
      const notificationOptions: NotificationOptions = {
        titleNotification: {
          text: 'El documento se ha cargado correctamente'
        },
        classes: notificationClasses,
        type: 'success',
        isDismissible: true
      };

      this.alertService.openAlert(notificationOptions, alertOptions).then();
    }
  }

  openHeaderNotificationCustom(caller: TemplateRef<any>, focusElement: string): void {
    const alertArea = this.alertAreasService.alertAreas.get(AlertAreasService.HEADER_NOTIFICATION_AREA_KEY);
    if (alertArea) {
      const alertOptions: AlertOptions = {
        id: 'alert',
        place: alertArea
      };

      this.alertService.openAlert(caller, alertOptions).then( result => {

        // Se añade al contexto del caller la propia alerta creada para poder cerrarla
        result.alert.instance.callerContext = {
          alertRef: result.alert
        };

        if (focusElement) {

          // Se debe esperar a que se cree la notificacion antes de hacer el focus
          setTimeout(() => {
            const elementToFocus = document.getElementById(focusElement);
            elementToFocus?.focus()
          }, 200);
        }
      });
    }
  }

  closeCustomAlert(alertRef: ComponentRef<AlertComponent>): void {
    this.alertService.closeAlert(alertRef);
  }

}
