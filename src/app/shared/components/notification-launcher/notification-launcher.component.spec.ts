import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationLauncherComponent } from './notification-launcher.component';

describe('NotificationLauncherComponent', () => {
  let component: NotificationLauncherComponent;
  let fixture: ComponentFixture<NotificationLauncherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotificationLauncherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationLauncherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
