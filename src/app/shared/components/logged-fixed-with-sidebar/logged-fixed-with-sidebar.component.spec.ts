import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LoggedFixedWithSidebarComponent } from './logged-fixed-with-sidebar.component';

describe('LoggedFixedWithSidebarComponent', () => {
  let component: LoggedFixedWithSidebarComponent;
  let fixture: ComponentFixture<LoggedFixedWithSidebarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoggedFixedWithSidebarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedFixedWithSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
