import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNotificationFooterComponent } from './page-notification-footer.component';

describe('PageNotificationFooterComponent', () => {
  let component: PageNotificationFooterComponent;
  let fixture: ComponentFixture<PageNotificationFooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageNotificationFooterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNotificationFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
