import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LoremipsumLargeComponent } from './loremipsum-large.component';


describe('LoremipsumLargeComponent', () => {
  let component: LoremipsumLargeComponent;
  let fixture: ComponentFixture<LoremipsumLargeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoremipsumLargeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoremipsumLargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
