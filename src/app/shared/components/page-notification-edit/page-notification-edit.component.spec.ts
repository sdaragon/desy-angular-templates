import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNotificationEditComponent } from './page-notification-edit.component';

describe('PageNotificationEditComponent', () => {
  let component: PageNotificationEditComponent;
  let fixture: ComponentFixture<PageNotificationEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageNotificationEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNotificationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
