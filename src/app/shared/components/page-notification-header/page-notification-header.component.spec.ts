import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNotificationHeaderComponent } from './page-notification-header.component';

describe('PageNotificationHeaderComponent', () => {
  let component: PageNotificationHeaderComponent;
  let fixture: ComponentFixture<PageNotificationHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageNotificationHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNotificationHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
