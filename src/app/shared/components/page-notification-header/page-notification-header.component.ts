import {Component, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {AlertAreasService} from '../../services/alert-areas.service';

@Component({
  selector: 'app-page-notification-header',
  templateUrl: './page-notification-header.component.html',
  styleUrls: ['./page-notification-header.component.css']
})
export class PageNotificationHeaderComponent implements OnInit, OnDestroy {

  @ViewChild('alertsArea', {read: ViewContainerRef, static: true}) alertsArea: ViewContainerRef | undefined;

  constructor(private alertsAreaService: AlertAreasService) {}

  ngOnInit(): void {
    if (this.alertsArea) {
      this.alertsAreaService.alertAreas.set(AlertAreasService.HEADER_NOTIFICATION_AREA_KEY, this.alertsArea);
    }
  }

  ngOnDestroy(): void {
    this.alertsAreaService.alertAreas.delete(AlertAreasService.HEADER_NOTIFICATION_AREA_KEY);
  }

  isAriaHidden(): boolean | null {
    return this.alertsArea && this.alertsArea.length > 0 ? null : true;
  }
}
