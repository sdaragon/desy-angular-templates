import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PageEditHeaderComponent } from './page-edit-header.component';


describe('PageEditHeaderComponent', () => {
  let component: PageEditHeaderComponent;
  let fixture: ComponentFixture<PageEditHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageEditHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageEditHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
