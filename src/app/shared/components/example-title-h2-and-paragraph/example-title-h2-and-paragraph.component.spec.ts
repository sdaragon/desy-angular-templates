import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExampleTitleH2AndParagraph } from './example-title-h2-and-paragraph.component';

describe('EjemploTituloH2ParrafoComponent', () => {
  let component: ExampleTitleH2AndParagraph;
  let fixture: ComponentFixture<ExampleTitleH2AndParagraph>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExampleTitleH2AndParagraph ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExampleTitleH2AndParagraph);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
