import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EditFixedWithStickySidebarComponent } from './edit-fixed-with-sticky-sidebar.component';

describe('EditFixedWithStickySidebarComponent', () => {
  let component: EditFixedWithStickySidebarComponent;
  let fixture: ComponentFixture<EditFixedWithStickySidebarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditFixedWithStickySidebarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFixedWithStickySidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
