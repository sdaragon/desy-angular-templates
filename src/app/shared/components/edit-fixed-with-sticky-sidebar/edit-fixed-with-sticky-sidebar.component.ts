import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-fixed-with-sticky-sidebar',
  templateUrl: './edit-fixed-with-sticky-sidebar.component.html',
  styleUrls: ['./edit-fixed-with-sticky-sidebar.component.css']
})
export class EditFixedWithStickySidebarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
