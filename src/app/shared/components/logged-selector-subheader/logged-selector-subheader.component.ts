import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-logged-selector-subheader',
  templateUrl: './logged-selector-subheader.component.html',
  styleUrls: ['./logged-selector-subheader.component.css']
})
export class LoggedSelectorSubheaderComponent implements OnInit {

  @Input() relativeRoute = '';

  constructor() {

  }

  ngOnInit(): void {
  }

}
