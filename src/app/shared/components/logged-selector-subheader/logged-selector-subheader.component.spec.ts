import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LoggedSelectorSubheaderComponent } from './logged-selector-subheader.component';

describe('LoggedSelectorSubheaderComponent', () => {
  let component: LoggedSelectorSubheaderComponent;
  let fixture: ComponentFixture<LoggedSelectorSubheaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoggedSelectorSubheaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedSelectorSubheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
