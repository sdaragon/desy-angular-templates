import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSpinnerBlockComponent } from './page-spinner-block.component';

describe('PageSpinnerBlockComponent', () => {
  let component: PageSpinnerBlockComponent;
  let fixture: ComponentFixture<PageSpinnerBlockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageSpinnerBlockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSpinnerBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
