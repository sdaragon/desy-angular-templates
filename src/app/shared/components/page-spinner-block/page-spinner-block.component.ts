import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-spinner-block',
  templateUrl: './page-spinner-block.component.html',
  styleUrls: ['./page-spinner-block.component.css']
})
export class PageSpinnerBlockComponent implements OnInit {

  @Input() text: string = '';
  @Input() loadedContent: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }
}
