import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NotificationEditInnerContentComponent } from './notification-edit-inner-content.component';


describe('NotificationEditInnerContentComponent', () => {
  let component: NotificationEditInnerContentComponent;
  let fixture: ComponentFixture<NotificationEditInnerContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotificationEditInnerContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationEditInnerContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
