import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-notification-edit-inner-content',
  templateUrl: './notification-edit-inner-content.component.html',
  styleUrls: ['./notification-edit-inner-content.component.css']
})
export class NotificationEditInnerContentComponent implements OnInit {

  @Input() isEdit = false

  constructor() { }

  ngOnInit(): void {
  }

}
