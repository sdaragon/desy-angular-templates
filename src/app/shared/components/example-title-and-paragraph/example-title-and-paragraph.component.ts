import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-example-title-and-paragraph',
  templateUrl: './example-title-and-paragraph.component.html',
  styleUrls: ['./example-title-and-paragraph.component.css']
})
export class ExampleTitleAndParagraphComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
