import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExampleTitleAndParagraphComponent } from './example-title-and-paragraph.component';

describe('EjemploTituloParrafoComponent', () => {
  let component: ExampleTitleAndParagraphComponent;
  let fixture: ComponentFixture<ExampleTitleAndParagraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExampleTitleAndParagraphComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExampleTitleAndParagraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
