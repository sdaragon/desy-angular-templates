import {Component, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {AlertAreasService} from '../../services/alert-areas.service';

@Component({
  selector: 'app-page-notification-inner-content',
  templateUrl: './page-notification-inner-content.component.html',
  styleUrls: ['./page-notification-inner-content.component.css']
})
export class PageNotificationInnerContentComponent implements OnInit, OnDestroy {

  @ViewChild('alertsArea', {read: ViewContainerRef, static: true}) alertsArea: ViewContainerRef | undefined;

  constructor(private alertsAreaService: AlertAreasService) {}

  ngOnInit(): void {
    if (this.alertsArea) {
      this.alertsAreaService.alertAreas.set(AlertAreasService.INNER_CONTENT_NOTIFICATION_AREA_KEY, this.alertsArea);
    }
  }

  ngOnDestroy(): void {
    this.alertsAreaService.alertAreas.delete(AlertAreasService.INNER_CONTENT_NOTIFICATION_AREA_KEY);
  }

  isAriaHidden(): boolean | null {
    return this.alertsArea && this.alertsArea.length > 0 ? null : true;
  }

}
