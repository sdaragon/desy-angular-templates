import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PageNotificationInnerContentComponent } from './page-notification-inner-content.component';


describe('PageNotificationInnerContentComponent', () => {
  let component: PageNotificationInnerContentComponent;
  let fixture: ComponentFixture<PageNotificationInnerContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageNotificationInnerContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNotificationInnerContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
