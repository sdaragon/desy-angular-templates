import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNotificationHeaderFixedComponent } from './page-notification-header-fixed.component';

describe('PageNotificationHeaderFixedComponent', () => {
  let component: PageNotificationHeaderFixedComponent;
  let fixture: ComponentFixture<PageNotificationHeaderFixedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageNotificationHeaderFixedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNotificationHeaderFixedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
