import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExampleTitleAndParagraphComponent } from "./components/example-title-and-paragraph/example-title-and-paragraph.component";
import { ExampleTitleH2AndParagraph } from "./components/example-title-h2-and-paragraph/example-title-h2-and-paragraph.component";
import { HeaderActionsComponent } from "./components/header-actions/header-actions.component";
import { LoremipsumLargeComponent } from "./components/loremipsum-large/loremipsum-large.component";
import { PageFooterComponent } from "./components/page-footer/page-footer.component";
import { PageNotificationFooterComponent } from "./components/page-notification-footer/page-notification-footer.component";
import { PageNotificationHeaderComponent } from "./components/page-notification-header/page-notification-header.component";
import { DesyAngularModule } from "desy-angular";
import { PageSpinnerBlockComponent } from "./components/page-spinner-block/page-spinner-block.component";
import { LoggedFixedWithSidebarComponent } from "./components/logged-fixed-with-sidebar/logged-fixed-with-sidebar.component";
import { LoggedSelectorSubheaderComponent } from "./components/logged-selector-subheader/logged-selector-subheader.component";
import { NotificationEditInnerContentComponent } from "./components/notification-edit-inner-content/notification-edit-inner-content.component";
import { PageNotificationInnerContentComponent } from "./components/page-notification-inner-content/page-notification-inner-content.component";
import { PageEditHeaderComponent } from "./components/page-edit-header/page-edit-header.component";
import { EditFixedWithStickySidebarComponent } from "./components/edit-fixed-with-sticky-sidebar/edit-fixed-with-sticky-sidebar.component";
import { NotificationLauncherComponent } from './components/notification-launcher/notification-launcher.component';
import { PageNotificationHeaderFixedComponent } from './components/page-notification-header-fixed/page-notification-header-fixed.component';
import { PageNotificationEditComponent } from './components/page-notification-edit/page-notification-edit.component';



@NgModule({
  declarations: [
    HeaderActionsComponent,
    ExampleTitleAndParagraphComponent,
    ExampleTitleH2AndParagraph,
    LoremipsumLargeComponent,
    PageFooterComponent,
    PageNotificationHeaderComponent,
    PageNotificationHeaderFixedComponent,
    PageNotificationFooterComponent,
    PageNotificationEditComponent,
    PageSpinnerBlockComponent,
    LoggedFixedWithSidebarComponent,
    LoggedSelectorSubheaderComponent,
    NotificationEditInnerContentComponent,
    PageNotificationInnerContentComponent,
    PageEditHeaderComponent,
    EditFixedWithStickySidebarComponent,
    NotificationLauncherComponent
  ],
  imports: [
    CommonModule,
    DesyAngularModule,
  ],
  exports: [
    HeaderActionsComponent,
    ExampleTitleAndParagraphComponent,
    ExampleTitleH2AndParagraph,
    LoremipsumLargeComponent,
    PageFooterComponent,
    PageNotificationHeaderComponent,
    PageNotificationHeaderFixedComponent,
    PageNotificationFooterComponent,
    PageNotificationEditComponent,
    PageSpinnerBlockComponent,
    LoggedFixedWithSidebarComponent,
    LoggedSelectorSubheaderComponent,
    NotificationEditInnerContentComponent,
    PageNotificationInnerContentComponent,
    PageEditHeaderComponent,
    EditFixedWithStickySidebarComponent,
    NotificationLauncherComponent
  ]
})
export class SharedModule { }
