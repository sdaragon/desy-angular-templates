import { TestBed } from '@angular/core/testing';

import { AlertAreasService } from './alert-areas.service';

describe('AlertAreasService', () => {
  let service: AlertAreasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AlertAreasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
