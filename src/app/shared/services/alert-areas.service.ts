import {Injectable, ViewContainerRef} from '@angular/core';

/**
 * Servicio cuyo fin es exponer las areas en las que se pueden añadir las notificaciones.
 */
@Injectable({
  providedIn: 'root'
})
export class AlertAreasService {

  public static readonly HEADER_NOTIFICATION_AREA_KEY = 'header-notification-area';
  public static readonly FOOTER_NOTIFICATION_AREA_KEY = 'footer-notification-area';
  public static readonly INNER_CONTENT_NOTIFICATION_AREA_KEY = 'inner-content-notification-area';

  public alertAreas: Map<string, ViewContainerRef> = new Map<string, ViewContainerRef>();

}
