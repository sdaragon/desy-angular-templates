import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from "./components/home/home.component";
import { HomeLayoutComponent } from './layouts/home/home-layout.component';
import { DesyAngularModule } from 'desy-angular';

@NgModule({
  declarations: [
    HomeComponent,
    HomeLayoutComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    DesyAngularModule
  ]
})
export class HomeModule { }
