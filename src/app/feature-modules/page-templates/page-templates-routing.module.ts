import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoggedLayoutComponent } from "./layouts/logged-layout/logged-layout.component";
import { LoggedOutLayoutComponent } from "./layouts/logged-out-layout/logged-out-layout.component";
import { PageExampleComponent } from "./components/page-example/page-example.component";
import {LoggedSelectorLayoutComponent} from "./layouts/logged-selector-layout/logged-selector-layout.component";
import {LoggedSelectorFixedLayoutComponent} from "./layouts/logged-selector-fixed-layout/logged-selector-fixed-layout.component";
import {PageExampleLargeComponent} from "./components/page-example-large/page-example-large.component";
import {LoggedSelectorFixedHeadroomLayoutComponent} from "./layouts/logged-selector-fixed-headroom-layout/logged-selector-fixed-headroom-layout.component";
import {LoggedSelectorSidebarLayoutComponent} from "./layouts/logged-selector-sidebar-layout/logged-selector-sidebar-layout.component";
import {PageExampleSidebarComponent} from "./components/page-example-sidebar/page-example-sidebar.component";
import {LoggedSelectorSubheaderLayoutComponent} from "./layouts/logged-selector-subheader-layout/logged-selector-subheader-layout.component";
import {EditSelectorFixedLayoutComponent} from "./layouts/edit-selector-fixed-layout/edit-selector-fixed-layout.component";
import {PageExampleLargeTitleH2Component} from "./components/page-example-large-title-h2/page-example-large-title-h2.component";
import {PageExampleSidebarStickyComponent} from "./components/page-example-sidebar-sticky/page-example-sidebar-sticky.component";
import {SpinnerComponent} from "./components/spinner/spinner.component";

const routes: Routes = [
  { path: '', redirectTo: 'logged-out', pathMatch: 'full' },
  {
    path: 'logged-out',
    component: LoggedOutLayoutComponent,
    children: [
        { path: '', component: PageExampleComponent },
        { path: 'spinner', component: SpinnerComponent}
        ]
  },
  {
    path: 'logged',
    component: LoggedLayoutComponent,
    children: [
        { path: '', component: PageExampleComponent },
        { path: 'spinner', component: SpinnerComponent }
    ]
  },
  {
    path: 'logged-app-selector',
    component: LoggedSelectorLayoutComponent,
    children: [
        { path: '', component: PageExampleComponent },
        { path: 'spinner', component: SpinnerComponent }]
  },
  {
    path: 'logged-app-selector-fixed',
    component: LoggedSelectorFixedLayoutComponent,
    children: [
      { path: '', component: PageExampleLargeComponent },
      { path: 'spinner', component: SpinnerComponent }]
  },
  {
    path: 'logged-app-selector-fixed-headroom',
    component: LoggedSelectorFixedHeadroomLayoutComponent,
    children: [{ path: '', component: PageExampleLargeComponent }]
  },
  {
    path: 'logged-app-selector-sidebar',
    component: LoggedSelectorSidebarLayoutComponent,
    children: [{ path: '', component: PageExampleSidebarComponent }]
  },
  {
    path: 'logged-app-selector-subheader',
    component: LoggedSelectorSubheaderLayoutComponent,
    runGuardsAndResolvers: 'always',
    children: [
      { path: '', component: PageExampleLargeComponent, data: { routeRelative: '' } },
      { path: 'subsection1', component: PageExampleLargeComponent, data: { routeRelative: '' } },
      { path: 'subsection2', component: PageExampleLargeComponent, data: { routeRelative: '' } },
      { path: 'subsection3', component: PageExampleLargeComponent, data: { routeRelative: '' } },
      { path: 'subsection4', component: PageExampleLargeComponent, data: { routeRelative: '' } },
      { path: 'spinner/subsection1', component: SpinnerComponent, data: { routeRelative: 'spinner' }},
      { path: 'spinner/subsection2', component: SpinnerComponent, data: { routeRelative: 'spinner' }},
      { path: 'spinner/subsection3', component: SpinnerComponent, data: { routeRelative: 'spinner' }},
      { path: 'spinner/subsection4', component: SpinnerComponent, data: { routeRelative: 'spinner' }},]
  },
  {
    path: 'edit-selector-fixed',
    component: EditSelectorFixedLayoutComponent,
    children: [
      { path: '', component: PageExampleLargeTitleH2Component },
      { path: 'spinner', component: SpinnerComponent }]
  },
  {
    path: 'edit-selector-fixed-sidebar-sticky',
    component: EditSelectorFixedLayoutComponent,
    children: [{ path: '', component: PageExampleSidebarStickyComponent }]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageTemplatesRoutingModule { }

