import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-example-large',
  templateUrl: './page-example-large.component.html',
  styleUrls: ['./page-example-large.component.css']
})
export class PageExampleLargeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
