import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageExampleLargeComponent } from './page-example-large.component';

describe('PageExampleLargeComponent', () => {
  let component: PageExampleLargeComponent;
  let fixture: ComponentFixture<PageExampleLargeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageExampleLargeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageExampleLargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
