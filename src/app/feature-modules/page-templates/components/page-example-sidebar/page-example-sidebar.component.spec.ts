import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageExampleSidebarComponent } from './page-example-sidebar.component';

describe('PageExampleSidebarComponent', () => {
  let component: PageExampleSidebarComponent;
  let fixture: ComponentFixture<PageExampleSidebarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageExampleSidebarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageExampleSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
