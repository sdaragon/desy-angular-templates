import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-page-example-sidebar-sticky',
  templateUrl: './page-example-sidebar-sticky.component.html',
  styleUrls: ['./page-example-sidebar-sticky.component.css']
})
export class PageExampleSidebarStickyComponent implements OnInit {

  title = 'Plantilla editar con cabecera fija y sidebar sticky. Gobierno de Aragón';

  constructor(private titleService: Title) {}

  setDocTitle(title: string) {
     this.titleService.setTitle(title);
  }

  ngOnInit(): void {
    this.setDocTitle(this.title);
  }

}
