import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageExampleSidebarStickyComponent } from './page-example-sidebar-sticky.component';

describe('PageExampleSidebarStickyComponent', () => {
  let component: PageExampleSidebarStickyComponent;
  let fixture: ComponentFixture<PageExampleSidebarStickyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageExampleSidebarStickyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageExampleSidebarStickyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
