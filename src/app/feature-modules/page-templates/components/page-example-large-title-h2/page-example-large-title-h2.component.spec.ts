import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageExampleLargeTitleH2Component } from './page-example-large-title-h2.component';

describe('PageExampleLargeTitleH2Component', () => {
  let component: PageExampleLargeTitleH2Component;
  let fixture: ComponentFixture<PageExampleLargeTitleH2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageExampleLargeTitleH2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageExampleLargeTitleH2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
