import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent implements OnInit {

  showSpinner: boolean = true;
  text: string = 'El contenido de la página se está cargando';
  loadedContent = false;

  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      this.changeText();
    }, 3000);
  }

  changeText(){
    this.text = "El contenido de la pagina se ha cargado";
    this.loadedContent = true;
    setTimeout(() => {
      this.quitSpinner();
    }, 5000);
  }

  quitSpinner(){
    this.showSpinner = false;
  }

}
