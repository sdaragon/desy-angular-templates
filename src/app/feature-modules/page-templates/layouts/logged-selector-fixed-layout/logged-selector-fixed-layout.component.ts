import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

/**
 * plantilla-logueado-con-cabecera-fija
 * Plantilla logueado con cabecera fija
 */
@Component({
  selector: 'app-logged-selector-fixed-layout',
  templateUrl: './logged-selector-fixed-layout.component.html',
  styleUrls: ['./logged-selector-fixed-layout.component.css']
})
export class LoggedSelectorFixedLayoutComponent implements OnInit {

  title = 'Plantilla logueado con cabecera fija. Gobierno de Aragón';

  constructor(private titleService: Title) {}

  setDocTitle(title: string) {
     this.titleService.setTitle(title);
  }

  ngOnInit(): void {
    this.setDocTitle(this.title);
  }

}
