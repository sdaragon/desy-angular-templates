import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoggedSelectorFixedLayoutComponent } from './logged-selector-fixed-layout.component';

describe('LoggedSelectorFixedLayoutComponent', () => {
  let component: LoggedSelectorFixedLayoutComponent;
  let fixture: ComponentFixture<LoggedSelectorFixedLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoggedSelectorFixedLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedSelectorFixedLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
