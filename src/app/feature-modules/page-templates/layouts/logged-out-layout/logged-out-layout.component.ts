import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

/**
 * plantilla-sin-loguear
 */
@Component({
  selector: 'app-logged-out-layout',
  templateUrl: './logged-out-layout.component.html',
  styleUrls: ['./logged-out-layout.component.css']
})
export class LoggedOutLayoutComponent implements OnInit {

  title = 'Plantilla sin loguear. Gobierno de Aragón';

  constructor(private titleService: Title) {}

  setDocTitle(title: string) {
     this.titleService.setTitle(title);
  }

  ngOnInit(): void {
    this.setDocTitle(this.title);
  }

}
