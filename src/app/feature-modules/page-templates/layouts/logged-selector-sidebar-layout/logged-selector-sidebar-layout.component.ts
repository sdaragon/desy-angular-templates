import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

/**
 * plantilla-logueado-con-selector-de-app-y-sidebar
 * Plantilla logueado con selector de app y sidebar
 */
@Component({
  selector: 'app-logged-selector-sidebar-layout',
  templateUrl: './logged-selector-sidebar-layout.component.html',
  styleUrls: ['./logged-selector-sidebar-layout.component.css']
})
export class LoggedSelectorSidebarLayoutComponent implements OnInit {

  title = 'Plantilla logueado con selector de app y sidebar. Gobierno de Aragón';

  constructor(private titleService: Title) {}

  setDocTitle(title: string) {
     this.titleService.setTitle(title);
  }

  ngOnInit(): void {
    this.setDocTitle(this.title);
  }

}
