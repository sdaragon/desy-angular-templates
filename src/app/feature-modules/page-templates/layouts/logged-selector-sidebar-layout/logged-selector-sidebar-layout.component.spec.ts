import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoggedSelectorSidebarLayoutComponent } from './logged-selector-sidebar-layout.component';

describe('LoggedSelectorSidebarLayoutComponent', () => {
  let component: LoggedSelectorSidebarLayoutComponent;
  let fixture: ComponentFixture<LoggedSelectorSidebarLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoggedSelectorSidebarLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedSelectorSidebarLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
