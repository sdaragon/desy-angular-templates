import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

/**
 * plantilla-logueado-con-selector-de-app
 * Plantilla logueado con selector de app. Gobierno de Aragón
 */
@Component({
  selector: 'app-logged-selector',
  templateUrl: './logged-selector-layout.component.html',
  styleUrls: ['./logged-selector-layout.component.css']
})
export class LoggedSelectorLayoutComponent implements OnInit {

  title = 'Plantilla logueado con selector de app. Gobierno de Aragón';

  constructor(private titleService: Title) {}

  setDocTitle(title: string) {
     this.titleService.setTitle(title);
  }

  ngOnInit(): void {
    this.setDocTitle(this.title);
  }

}
