import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoggedSelectorLayoutComponent } from './logged-selector-layout.component';

describe('LoggedSelectorComponent', () => {
  let component: LoggedSelectorLayoutComponent;
  let fixture: ComponentFixture<LoggedSelectorLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoggedSelectorLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedSelectorLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
