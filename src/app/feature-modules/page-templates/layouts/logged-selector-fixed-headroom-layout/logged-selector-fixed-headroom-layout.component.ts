import {Component, OnInit} from '@angular/core';
import { Title } from '@angular/platform-browser';

/**
 * plantilla-logueado-con-cabecera-fija-con-libreria-headroom
 * Plantilla logueado con cabecera fija con librería headroom.js
 */
@Component({
  selector: 'app-logged-selector-fixed-headroom-layout',
  templateUrl: './logged-selector-fixed-headroom-layout.component.html',
  styleUrls: ['./logged-selector-fixed-headroom-layout.component.css']
})
export class LoggedSelectorFixedHeadroomLayoutComponent implements OnInit {

  title = 'Plantilla logueado con cabecera fija con librería headroom.js. Gobierno de Aragón';

  constructor(private titleService: Title) {}

  setDocTitle(title: string) {
     this.titleService.setTitle(title);
  }

  ngOnInit(): void {
    this.setDocTitle(this.title);
  }

}
