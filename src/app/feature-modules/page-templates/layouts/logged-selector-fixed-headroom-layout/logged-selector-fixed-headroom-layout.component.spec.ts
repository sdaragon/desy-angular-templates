import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoggedSelectorFixedHeadroomLayoutComponent } from './logged-selector-fixed-headroom-layout.component';

describe('LoggedSelectorFixedHeadroomLayoutComponent', () => {
  let component: LoggedSelectorFixedHeadroomLayoutComponent;
  let fixture: ComponentFixture<LoggedSelectorFixedHeadroomLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoggedSelectorFixedHeadroomLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedSelectorFixedHeadroomLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
