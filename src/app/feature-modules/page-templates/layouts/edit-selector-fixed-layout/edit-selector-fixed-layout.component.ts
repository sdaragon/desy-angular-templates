import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

/**
 * plantilla-editar-con-cabecera-fija
 * Plantilla editar con cabecera fija
 */
@Component({
  selector: 'app-edit-selector-fixed-layout',
  templateUrl: './edit-selector-fixed-layout.component.html',
  styleUrls: ['./edit-selector-fixed-layout.component.css']
})
export class EditSelectorFixedLayoutComponent implements OnInit {

  title = 'Plantilla editar con cabecera fija. Gobierno de Aragón';

  constructor(private titleService: Title) {}

  setDocTitle(title: string) {
     this.titleService.setTitle(title);
  }

  ngOnInit(): void {
    this.setDocTitle(this.title);
  }

}
