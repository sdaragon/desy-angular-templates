import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSelectorFixedLayoutComponent } from './edit-selector-fixed-layout.component';

describe('EditSelectorFixedLayoutComponent', () => {
  let component: EditSelectorFixedLayoutComponent;
  let fixture: ComponentFixture<EditSelectorFixedLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditSelectorFixedLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSelectorFixedLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
