import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoggedSelectorSubheaderLayoutComponent } from './logged-selector-subheader-layout.component';

describe('LoggedSelectorSubheaderLayoutComponent', () => {
  let component: LoggedSelectorSubheaderLayoutComponent;
  let fixture: ComponentFixture<LoggedSelectorSubheaderLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoggedSelectorSubheaderLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedSelectorSubheaderLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
