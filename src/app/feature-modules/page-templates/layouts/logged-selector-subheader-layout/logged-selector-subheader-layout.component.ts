import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

/**
 * plantilla-logueado-con-selector-de-app-y-subheader
 * Plantilla logueado con selector de app y subheader
 */
@Component({
  selector: 'app-logged-selector-subheader-layout',
  templateUrl: './logged-selector-subheader-layout.component.html',
  styleUrls: ['./logged-selector-subheader-layout.component.css']
})
export class LoggedSelectorSubheaderLayoutComponent implements OnInit {

  subheaderRelativeRoute = './';
  title = 'Plantilla logueado con selector de app y subheader. Gobierno de Aragón';

  constructor(private activatedRoute: ActivatedRoute, private titleService: Title) {

  }

  setDocTitle(title: string) {
     this.titleService.setTitle(title);
  }

  ngOnInit(): void {
    const routeRelative = this.activatedRoute.firstChild?.snapshot.data.routeRelative;
    this.subheaderRelativeRoute = routeRelative ? routeRelative : './';
    this.setDocTitle(this.title);
  }

}
