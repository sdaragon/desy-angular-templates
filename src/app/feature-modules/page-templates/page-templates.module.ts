import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoggedLayoutComponent } from './layouts/logged-layout/logged-layout.component';
import { SharedModule } from "../../shared/shared.module";
import { PageTemplatesRoutingModule } from "./page-templates-routing.module";
import { LoggedOutLayoutComponent } from './layouts/logged-out-layout/logged-out-layout.component';
import { PageExampleComponent } from './components/page-example/page-example.component';
import { LoggedSelectorLayoutComponent } from './layouts/logged-selector-layout/logged-selector-layout.component';
import { DesyAngularModule } from 'desy-angular';
import { LoggedSelectorFixedLayoutComponent } from './layouts/logged-selector-fixed-layout/logged-selector-fixed-layout.component';
import { PageExampleLargeComponent } from './components/page-example-large/page-example-large.component';
import { LoggedSelectorFixedHeadroomLayoutComponent } from './layouts/logged-selector-fixed-headroom-layout/logged-selector-fixed-headroom-layout.component';
import { LoggedSelectorSidebarLayoutComponent } from './layouts/logged-selector-sidebar-layout/logged-selector-sidebar-layout.component';
import { PageExampleSidebarComponent } from './components/page-example-sidebar/page-example-sidebar.component';
import { LoggedSelectorSubheaderLayoutComponent } from './layouts/logged-selector-subheader-layout/logged-selector-subheader-layout.component';
import { EditSelectorFixedLayoutComponent } from './layouts/edit-selector-fixed-layout/edit-selector-fixed-layout.component';
import { PageExampleLargeTitleH2Component } from './components/page-example-large-title-h2/page-example-large-title-h2.component';
import { PageExampleSidebarStickyComponent } from './components/page-example-sidebar-sticky/page-example-sidebar-sticky.component';
import { SpinnerComponent } from "./components/spinner/spinner.component";
import { HeadroomModule } from '@ctrl/ngx-headroom'

@NgModule({
    declarations: [
        PageExampleComponent,
        PageExampleLargeComponent,
        PageExampleSidebarComponent,
        SpinnerComponent,

        LoggedLayoutComponent,
        LoggedOutLayoutComponent,
        LoggedSelectorLayoutComponent,
        LoggedSelectorFixedLayoutComponent,
        LoggedSelectorFixedHeadroomLayoutComponent,
        LoggedSelectorSidebarLayoutComponent,
        LoggedSelectorSubheaderLayoutComponent,
        EditSelectorFixedLayoutComponent,
        PageExampleLargeTitleH2Component,
        PageExampleSidebarStickyComponent
    ],
    imports: [
        CommonModule,
        PageTemplatesRoutingModule,
        SharedModule,
        DesyAngularModule,
        HeadroomModule
    ]
})
export class PageTemplatesModule { }
