import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';

const routes: Routes = [
  // Por defecto, que redirija a la página de home
  { path: '', loadChildren: () => import('./feature-modules/home/home.module').then(m => m.HomeModule), pathMatch: 'full' },
  { path: 'page-templates', loadChildren: () => import('./feature-modules/page-templates/page-templates.module').then(m => m.PageTemplatesModule) },
];

const routerOptions: ExtraOptions = {
  useHash: false,
  // ...any other options you'd like to use
};

@NgModule({
  imports: [RouterModule.forRoot(routes, routerOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

