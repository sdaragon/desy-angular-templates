
/**
 * Custom gulp file to generate an index.html file which uses desy-html and a reduced css file to display some content
 * before application loads (like an initial spinner).
 */

const gulp = require('gulp');
const { series } = require('gulp');
const clean = require('gulp-clean');
const nunjucksRender = require('gulp-nunjucks-render');
const postcss = require('gulp-postcss');
const atimport = require('postcss-import');
const tailwindnesting = require('tailwindcss/nesting');
const stripCssComments = require('gulp-strip-css-comments');
const footer = require('gulp-footer');
const tailwindcss = require('tailwindcss');
const autoprefixer = require('autoprefixer');
const rename = require('gulp-rename');

/* Load Desy tailwindcss config */
const defaultTailwindConfig = require('./tailwind.config.js');

const SOURCE_STYLESHEET = './node_modules/desy-html/src/css/styles.css';     // Inherited css from desy-html
const SOURCE_NUNJUCKS_PATHS = ['./node_modules/desy-html/src/templates/'];   // Nunjunks from desy-html
const SOURCE_NUNJUCKS_DIR = ['./src/index.html'];                            // index file

/*
 * Output path for processed index.html. Must be selected in angular.json as index
 */
const DESTINATION_HTML_DIR = './build/';

/*
 * Output path for processed css. Must be distributed as static file.
 */
const DESTINATION_STYLESHEET_DIR = './src/assets/generated/css/';

/*
 * Output css file name. Must be required in index.html.
 */
const DESTINATION_STYLESHEET = 'index-styles.css';

// TAILWIND CONFIG. You can separate this to other file, like main tailwind.config.js
const TAILWIND_CONFIG = {
    ...defaultTailwindConfig,

    /* Change PurgeCSS files to generate only the classes used by spinner in index.html */
    purge: {
        enabled: true,
        mode: 'layers',
        layers: [ 'base', 'components', 'utilities' ],
        content:[
            '../../src/index.html'
        ],
        options: {
            safelist: [
                'dev'
            ],
        },
    }
};


/**
 * Generates a css file for index.html
 */
function css() {
  return gulp.src(SOURCE_STYLESHEET)
    .pipe(
      postcss([
      atimport(),
      tailwindnesting(),
      tailwindcss(TAILWIND_CONFIG),
      autoprefixer()
      ])
    )
    .pipe(stripCssComments({preserve: false}))
    .pipe(footer('\n'))
    .pipe(rename(DESTINATION_STYLESHEET))
    .pipe(gulp.dest(DESTINATION_STYLESHEET_DIR));
}

/**
 * Generates a html with parsed desy-html nunjunks
 */
function nunjucks() {
  return gulp.src(SOURCE_NUNJUCKS_DIR)
    .pipe(nunjucksRender({
        envOptions: {
          autoescape: true,
          trimBlocks: true,
          lstripBlocks: true,
          noCache: true
        },
        path: SOURCE_NUNJUCKS_PATHS // String or Array
      }))
    .pipe(gulp.dest(DESTINATION_HTML_DIR));
}

/**
 * Clears the output html folder before start the gulp process
 */
function cleanFolder() {
  return gulp.src(DESTINATION_HTML_DIR, {read: false, allowEmpty: true}).pipe(clean());
}

/*
 * Gulp pipeline
 */
exports.default = series(cleanFolder, nunjucks, css);
