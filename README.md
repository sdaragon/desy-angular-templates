# Welcome to desy-angular-templates 👋
![Version](https://img.shields.io/badge/version-9.0.0-blue.svg?cacheSeconds=2592000)
![Prerequisite](https://img.shields.io/badge/npm-%3E%3D8.3-blue.svg)
![Prerequisite](https://img.shields.io/badge/node-%3E%3D16.14.0-blue.svg)
[![License: EUPL--1.2](https://img.shields.io/badge/License-EUPL--1.2-yellow.svg)](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)

> desy-angular-templates is a starter project to build user interfaces for Gobierno de Aragón government webapps, using desy-angular npm package as dependecy.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.0.


### 🏠 [Homepage](https://desy.aragon.es/desy-angular)

## Prerequisites

- npm >=8.3
- node >=16.14.0

## Install

```sh
npm install
```

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project.

Run `npm run build-prod` for a production build.

To consider other environments, you can add a custom configuration to angular.json and a new script to package.json similar to `build-prod`.

The build artifacts will be stored in the `dist/` directory. The `build/` autogenerated directory exists just for store temporal files needed before `ng build` task.

## Author

👤 **Desy (SDA Servicios Digitales de Aragón)**


## Show your support

Give a ⭐️ if this project helped you!


## 📝 License

This project is [EUPL--1.2](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12) licensed.
